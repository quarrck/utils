package helper

func Keys(m map[string]interface{}) []string {
	var ret []string
	for k := range m {
		ret = append(ret, k)
	}
	return ret
}
