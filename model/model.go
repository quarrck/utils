package model

type MSG struct {
	ID      string
	Type    string
	From    string
	To      []string
	Content string
}
